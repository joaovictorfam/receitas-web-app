** Aplicação web React **

Pequena SPA construida em React, capaz de realizar consultas ao servico local de receitas (receitas-api).
Exibe na tela uma lista com receitas de acordo com a temperatura da cidade procurada.

*Esta aplicação usa a API receitas-api pela porta 5000 do servidor local (http:localhost:5000)*

---

## Download & build local

Primeiro certifique-se de você possuí a versão mais recente do Node Instalado em sua máquina.
*(https://nodejs.org/)*

*Execute os comandos a seguir para subir o servidor localmente*

1. git clone https://joaovictorfam@bitbucket.org/joaovictorfam/receitas-web-app.git
2. cd receitas-web-app
3. npm install
4. npm start

---

O servidor deverá estar escutando na porta 3000 do localhost

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.