import React, {useEffect, useState} from 'react';
import Recipe from './Recipe';
import './App.css';
import CityNotFound from './CityNotFound';
import TooManyRequests from './TooManyRequests';

function App() {  
  const [requestError, setRequestError] = useState('');
  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState('');

  useEffect(() => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(async function(position) {
        await getCity(position.coords.latitude, position.coords.longitude)
      });
    }
  }, [])
    
  useEffect(() => {
    if(query)
      getRecipes();
  }, [query])

  const getCity = async (lat, lon) => {
    const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lon}&key=AIzaSyBDDmk6z_eO-pGw4SnJfql0WMirV3IoH3c`);
    const data = await response.json();
    if(data){
      const str = data.plus_code.compound_code;
      const location = str.substr(str.indexOf(' ')+1);
      const city = location.substr(0, location.indexOf(','));
      setSearch(city);
      setQuery(city);
    }
  }

  const getRecipes = async () => {
    const response = await fetch(`http://localhost:5000/cidades/${query}`).then(function(res) {
      if(res.ok)
        return res.json();
      if(res.status == 404)
        throw 404;
      if(res.status == 429)
        throw 429;
      throw 500;
    }).then(function(res) {
        if(res) {
          console.log(res.hits);
          setRecipes(res.hits);
          setRequestError('');
        }
    }).catch(function(error) {
      setRequestError(error);
      console.log('There has been a problem with your fetch operation: ', error);
    });
  }
        
  const updateSearch = e => {
    setSearch(e.target.value);
  }

  const getSearch = e => {
    e.preventDefault();
    setQuery(search);
    setSearch('');
  }

  let itemToRender;
  if(requestError) {
    if(requestError == 404)
      itemToRender = <CityNotFound />
    if(requestError == 429)
      itemToRender = <TooManyRequests />
  } else if(recipes) {
    itemToRender = recipes.map((recipe, i) => (
      <Recipe key={i}
        titulo={recipe.recipe.label}
        calorias={recipe.recipe.calories}
        cautions={recipe.recipe.cautions}
        dietLabels={recipe.recipe.dietLabels}
        healthLabels={recipe.recipe.healthLabels}
        image={recipe.recipe.image}
        igredientsLines={recipe.recipe.igredientsLines}
        ingredientes={recipe.recipe.ingredients}
        url={recipe.recipe.url}
      />
    ));
  }

  return (
    <div className="App">
      <form onSubmit={getSearch} className="search-form">
        <input className="search-bar" type="text" value={search} onChange={updateSearch}/>
        <button className="search-button" type="submit">Pesquisar</button>
      </form>
      
      <section className="recipes">
        {itemToRender}
      </section>
    </div>
  );

}

export default App;
