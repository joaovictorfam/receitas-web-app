import React from 'react';

const CityNotFound = () => {
    return(
        <div>
            <h1>Não encontramos a cidade que você procura</h1>
            <h3>Status: <strong>404</strong></h3>
        </div>
    );
}

export default CityNotFound;