import React from 'react';
import style from './recipe.module.css';

const Recipe = ({titulo, calorias, cautions, dietLabels, healthLabels, image, ingredientsLines, ingredientes, url}) => {
    return(
        <div className={style.recipe}>
            <div className={style.recipeImg}>
                <img className={style.img} src={image} alt="" />
            </div>
            <div className={style.recipeDetails}>
                <h3 className={style.recipeTitle}>{titulo}</h3>
                <p className={style.recipeInstruction}>
                    {ingredientes.map((ingrediente, i) => (
                        <li key={i}>{ingrediente.text}</li>
                    ))}
                </p>
                <ul className={style.recipeInfo}>
                    <li>
                        Calorias:
                        <strong>{Math.floor(calorias)}</strong>
                    </li>
                    <li>
                        Saúde:
                        <strong>{healthLabels}</strong>
                    </li>
                </ul>
                <form action={url}>
                    <button className={style.btn}>
                        Ver Receita
                    </button>
                </form>
            </div>
        </div>
    );
}

export default Recipe;