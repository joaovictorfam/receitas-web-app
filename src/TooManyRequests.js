import React from 'react';

const CityNotFound = () => {
    return(
        <div>
            <h1>Você fez muitas requisições, por favor aguarde um minuto.</h1>
            <h3>Status: <strong>429</strong></h3>
        </div>
    );
}

export default CityNotFound;